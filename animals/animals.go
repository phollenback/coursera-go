package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

type Animal struct {
	food       string
	locomotion string
	noise      string
}

func (animal Animal) Eat() {
	fmt.Println(animal.food)
}

func (animal Animal) Move() {
	fmt.Println(animal.locomotion)
}

func (animal Animal) Speak() {
	fmt.Println(animal.noise)
}

func main() {
	var cow = Animal{"grass", "walk", "moo"}
	var bird = Animal{"worms", "fly", "peep"}
	var snake = Animal{"mice", "slither", "hiss"}

	var animal Animal

	scanner := bufio.NewScanner(os.Stdin)

	var name string
	var action string

	fmt.Println("Enter animal and action")
	fmt.Println("valid animals: cow, bird, snake")
	fmt.Println("valid actions: eat, speak, move")

	for {
		fmt.Print("> ")
		scanner.Scan()
		answer := strings.Split(scanner.Text(), " ")
		name = answer[0]
		action = answer[1]

		switch name {
		case "cow":
			animal = cow
		case "bird":
			animal = bird
		case "snake":
			animal = snake
		}

		switch action {
		case "eat":
			animal.Eat()
		case "speak":
			animal.Speak()
		case "move":
			animal.Move()
		}
	}

}
