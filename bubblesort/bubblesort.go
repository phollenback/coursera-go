package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

// take a slice of integers and bubble sort it in place
func BubbleSort(unsorted []int) {

	length := len(unsorted)
	for i := 0; i < (length - 1); i++ {
		for j := 0; j < (length - i - 1); j++ {
			if unsorted[j] > unsorted[j+1] {
				Swap(unsorted, j)
			}
		}
	}
	for _, k := range unsorted {
		fmt.Print(k)
		fmt.Print(" ")
	}
	fmt.Println()

}

// swap two members of a slice in place
func Swap(sli []int, i int) {
	sli[i], sli[i+1] = sli[i+1], sli[i]
}

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	numSlice := []int{}

	fmt.Print("enter up to 10 integers, space separated: ")
	// use string split to get all the ints
	scanner.Scan()
	nums := scanner.Text()
	// strArray is an array of strings
	strArray := strings.Split(nums, " ")
	for _, num := range strArray {
		num2, _ := strconv.Atoi(num)
		numSlice = append(numSlice, num2)
	}
	if len(numSlice) > 10 {
		fmt.Println("error: too many numbers")
		os.Exit(1)
	}

	BubbleSort(numSlice)
}
