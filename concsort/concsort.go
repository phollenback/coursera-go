package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
	"sync"
)

/*
Write a program to sort an array of integers. The program should
partition the array into 4 parts, each of which is sorted by a
different goroutine. Each partition should be of approximately equal
size. Then the main goroutine should merge the 4 sorted subarrays
into one large sorted array.

The program should prompt the user to input a series of integers.
Each goroutine which sorts ¼ of the array should print the subarray
that it will sort. When sorting is complete, the main goroutine
should print the entire sorted list.
*/

// request list of integers from user and return it as
// a slice of ints.
func enterIntegers() []int {
	nums := []int{}
	scanner := bufio.NewScanner(os.Stdin)

	fmt.Print("Enter list of integers: ")
	scanner.Scan()
	numsAsText := strings.Split(scanner.Text(), " ")

	for _, s := range numsAsText {
		num, _ := strconv.Atoi(s)
		nums = append(nums, num)
	}
	return nums
}

func splitRange(nums []int) ([]int, []int, []int, []int) {

	// split the input in to four approximately equal sets
	q1 := []int{}
	q2 := []int{}
	q3 := []int{}
	q4 := []int{}

	space := len(nums)
	quarter := space / 4

	start1 := 0
	start2 := (start1 + quarter)
	start3 := (start1 + (quarter * 2))
	start4 := (start1 + (quarter * 3))

	for i := 0; i < space; i++ {
		if i >= start1 && i < start2 {
			q1 = append(q1, nums[i])
		} else if i >= start2 && i < start3 {
			q2 = append(q2, nums[i])
		} else if i >= start3 && i < start4 {
			q3 = append(q3, nums[i])
		} else {
			q4 = append(q4, nums[i])
		}
	}
	return q1, q2, q3, q4
}

// sort a list of ints and return them, in a concurrent manner
func sortChunk(nums []int, wg *sync.WaitGroup) {
	defer wg.Done()
	printChunk("sorting:", nums)
	sort.Ints(nums)
}

// print out what we are going to sort
func printChunk(msg string, chunk []int) {
	fmt.Print(msg + " ")
	for _, i := range chunk {
		fmt.Print(i)
		fmt.Print(" ")
	}
	fmt.Println()
}

func main() {
	nums := enterIntegers()

	q1, q2, q3, q4 := splitRange(nums)

	var wg sync.WaitGroup
	wg.Add(4)

	go sortChunk(q1, &wg)
	go sortChunk(q2, &wg)
	go sortChunk(q3, &wg)
	go sortChunk(q4, &wg)

	wg.Wait()

	sortedSlice := append(q1, q2...)
	sortedSlice = append(sortedSlice, q3...)
	sortedSlice = append(sortedSlice, q4...)

	wg.Add(1)
	sortChunk(sortedSlice, &wg)
	wg.Wait()

	printChunk("sorted:", sortedSlice)
}
