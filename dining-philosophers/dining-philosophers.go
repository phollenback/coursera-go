package main

/*

Implement the dining philosopher’s problem with the following
constraints/modifications.

There should be 5 philosophers sharing chopsticks, with one chopstick
between each adjacent pair of philosophers.

Each philosopher should eat only 3 times (not in an infinite loop as
we did in lecture)

The philosophers pick up the chopsticks in any order, not
lowest-numbered first (which we did in lecture).

In order to eat, a philosopher must get permission from a host which
executes in its own goroutine.

The host allows no more than 2 philosophers to eat concurrently.  Each
philosopher is numbered, 1 through 5.

When a philosopher starts eating (after it has obtained necessary
locks) it prints “starting to eat <number>” on a line by itself, where
<number> is the number of the philosopher.

When a philosopher finishes eating (before it has released its locks)
it prints “finishing eating <number>” on a line by itself, where
<number> is the number of the philosopher.
*/

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

// need 5 chopsticks
var chopstick [5]int

var wg sync.WaitGroup

type Host struct{ sync.Mutex }

var host Host

type PhilosopherReq struct {
	id     int
	pickUp bool
}

var concurrent int

// the host listens forever for requests from philosophers
// that want to eat
func (Host) Listen(req <-chan PhilosopherReq, resp chan<- bool) {
	for {
		philo := <-req
		//		fmt.Printf("got req for %v\n", philo.id)
		myRand := rand.New(rand.NewSource(time.Now().UnixNano()))
		// lC and rC are the left and right
		// chopstick numbers for the philosopher
		var lC, rC = philo.id, (philo.id + 1) % 5
		var hasValue = 1
		if philo.pickUp {
			hasValue = 0
		}
		if chopstick[lC] == hasValue &&
			chopstick[rC] == hasValue {
			// randomly pick up left or right
			// chopstick first
			if myRand.Intn(2) == 0 {
				chopstick[lC] = (hasValue + 1) % 2
				chopstick[rC] = (hasValue + 1) % 2
			} else {
				chopstick[rC] = (hasValue + 1) % 2
				chopstick[lC] = (hasValue + 1) % 2
			}
			//			fmt.Println(chopstick)
			resp <- true
			continue
		}
		//		fmt.Println(chopstick)
		resp <- false
	}
}

// action the philosopher actually performs - start or stop eating
func Action(philosopherReq PhilosopherReq,
	req chan<- PhilosopherReq, resp <-chan bool) {

	//	fmt.Printf("in Action, philo %v, pickUp %v\n", philosopherReq.id, philosopherReq.pickUp)
	for {
		host.Lock()
		if concurrent > 1 && philosopherReq.pickUp == true {
			//			fmt.Printf("%v can't start eating because %v are already eating\n", philosopherReq.id, concurrent)
			host.Unlock()
			continue
		}
		req <- philosopherReq
		//		fmt.Printf("in Action, philo %v, pickUp %v\n", philosopherReq.id, philosopherReq.pickUp)
		if <-resp {
			if philosopherReq.pickUp {
				// this was a pickup req, inc counter
				concurrent++
			} else {
				// this was a drop req, dec counter
				concurrent--
			}
			//			fmt.Printf("concurrent is %v\n", concurrent)
			host.Unlock()
			break
		}
		host.Unlock()
	}
}

// a philosopher loops 3 times sending requests to the host
func Philosopher(i int, req chan<- PhilosopherReq, resp <-chan bool) {
	defer wg.Done()
	var philosopherReq PhilosopherReq
	philosopherReq.id = i

	for j := 0; j < 3; j++ {
		philosopherReq.pickUp = true

		Action(philosopherReq, req, resp)
		fmt.Printf("starting to eat %v\n", i+1)
		//		fmt.Printf("%v sleeping\n", i)
		//		time.Sleep(time.Duration(rand.Intn(1000)) * time.Millisecond)
		philosopherReq.pickUp = false
		//		fmt.Printf("ready to put down chopsticks for %v\n", i)
		Action(philosopherReq, req, resp)
		fmt.Printf("finished eating %v\n", i+1)
	}
}

func main() {
	req := make(chan PhilosopherReq)
	resp := make(chan bool)

	// start the host listening - don't add the host to the
	// waitgroup because we want it to die when all requests are
	// done anyway
	go host.Listen(req, resp)

	// seat the philosophers
	for i := 0; i < 5; i++ {
		wg.Add(1)
		go Philosopher(i, req, resp)
	}
	wg.Wait()
}
