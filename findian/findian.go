package main

import (
	"strings"
	"fmt"
	"bufio"
	"os"
)

func main(){
	var s, l, n string

	reader := bufio.NewReader(os.Stdin)

	fmt.Print("Enter a string: ")
	s, _ = reader.ReadString('\n')
	n = strings.Replace(s, "\n", "", -1)
	l = strings.ToLower(n)
	if strings.HasPrefix(l, "i") &&
		strings.HasSuffix(l, "n") &&
		strings.Contains(l, "a"){
				fmt.Println("Found!")
	} else {
		fmt.Println("Not Found!")
	}
}
