package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

// GenDisplace64 takes 3 floats and returns a func that takes one
// float and returns a float
func GenDisplaceFn(a float64, v0 float64, s0 float64) func(float64) float64 {
	fn := func(t float64) float64 {
		s := .5*a*(t*t) + v0*t + s0
		return s
	}
	return fn
}

func main() {
	scanner := bufio.NewScanner(os.Stdin)

	fmt.Print("enter acceleration: ")
	scanner.Scan()
	aTxt := scanner.Text()

	fmt.Print("enter inital velocity: ")
	scanner.Scan()
	v0Txt := scanner.Text()

	fmt.Print("enter initial displacement: ")
	scanner.Scan()
	s0Txt := scanner.Text()

	a, _ := strconv.ParseFloat(aTxt, 64)
	v0, _ := strconv.ParseFloat(v0Txt, 64)
	s0, _ := strconv.ParseFloat(s0Txt, 64)

	fn := GenDisplaceFn(a, v0, s0)

	fmt.Print("enter time: ")
	scanner.Scan()
	tTxt := scanner.Text()

	t, _ := strconv.ParseFloat(tTxt, 64)

	fmt.Print("displacement after ")
	fmt.Print(t)
	fmt.Print(": ")
	fmt.Println(fn(t))
}
