package main

import (
	"encoding/json"
	"fmt"
	"bufio"
	"os"
)

/* Write a program which prompts the user to first enter a name, and
then enter an address. Your program should create a map and add the
name and address to the map using the keys “name” and “address”,
respectively. Your program should use Marshal() to create a JSON
object from the map, and then your program should print the JSON
object.  */

func main(){
	myAddrs := make(map[string]string)
	scanner := bufio.NewScanner(os.Stdin)

	fmt.Print("Enter a name: ")
	scanner.Scan()
	myAddrs["name"] = scanner.Text()

	fmt.Print("Enter an address: ")
	scanner.Scan()
	myAddrs["address"] = scanner.Text()

	json, _ := json.Marshal(myAddrs)

	fmt.Println()
	fmt.Println(string(json))
}
