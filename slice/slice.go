package main

import(
	"fmt"
	"strings"
	"strconv"
	"sort"
	"os"
)

func main(){
	unSortSlice := make([]int,3)
	sortSlice := make([]int,3)

	// infinite loop!
	for i:=0; ;i++ {
		inputString := ""
		fmt.Print("Enter an integer, or X to quit: ")
		fmt.Scanln(&inputString)
		if strings.Compare(inputString, "X") == 0 {
			break
		}
		inputInt, err := strconv.Atoi(inputString)
		if err != nil {
			fmt.Fprintf(os.Stderr, "not a valid integer: %s\n", err)
			continue
		}
		if i < 3 {
			unSortSlice[i] = inputInt
		} else {
			unSortSlice = append(unSortSlice,inputInt)
			sortSlice = append(sortSlice,inputInt)
		}
		copy(sortSlice,unSortSlice)
		sort.Ints(sortSlice)
		fmt.Println(sortSlice)
	}

}
