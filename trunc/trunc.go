package main

import (
  "fmt"
  "math"
)

func main(){
  var f float64;
  var i int;
  fmt.Print("Enter a float: ")
  fmt.Scanf("%f", &f);
  i = int(math.Trunc(f))
  fmt.Println(i)
}
